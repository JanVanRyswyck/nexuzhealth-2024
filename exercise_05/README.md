# Birthday Greetings

## 1. Description

As you’re a very friendly employer, you would like to send a birthday note to all your employees. But you have a 
lot of employees and as you're also a bit lazy, it may take some times to write all the notes by hand. The good news is 
that computers can do it automatically for you.

Imagine you have a flat file with all your employees:

```
last_name, first_name, date_of_birth, email
Doe, John, 1982/10/08, john.doe@foobar.com
Ann, Mary, 1975/09/11, mary.ann@foobar.com
```

And you want to send them a happy birthday email on their birthdate:

```
Subject: Happy birthday!
Happy birthday, dear <first_name>. Have a wonderful day.
```

## 2. Solitary tests

1. Open the files `pkg/domain/birthday_greetings_handler.go` and `tests/solitary/birthday_greetings_handler_test.go`.
2. Start with the first Solitary test from the list, following a Test-First approach ("Red, Green, Refactor").
   * Implement only a single test case at a time. Make sure that the test fails.
   * Provide the least amount of code to make the test pass.
   * Refactor, refactor, refactor!
   * Rinse & repeat.

## 3. Sociable test

1. Open the file `tests/sociable/send_birthday_greetings_test.go`.
2. Implement a Sociable test to make sure that all the ports and adapters can work together correctly.

## 4. Open questions

* In exercise 1, you've used Outside-In TDD to flesh out the different parts of the  feature, while in this exercise 
you've used Inside-Out TDD. What are your thoughts about these approaches?
* Where did you implement the logic of determining whether it's an employee's birthday? In the `BirthdayGreetingsHandler`
or the `Employee` itself? Why did you make this choice?
* What are your thoughts about the Hexagonal Architecture? What makes this design approach testable?
