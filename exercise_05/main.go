package main

import (
	"exercise_05/pkg"
	"os"
	"time"
)

func main() {
	currentDate := time.Now().Truncate(24 * time.Hour)
	employeesFileName := os.Args[1:][0]

	pkg.ExecuteProgram(currentDate, employeesFileName)
}
