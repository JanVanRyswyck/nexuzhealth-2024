package ports

import "time"

type EmployeeData struct {
	LastName    string
	FirstName   string
	DateOfBirth time.Time
	Email       string
}

type EmployeeRepository interface {
	Get() ([]EmployeeData, error)
}
