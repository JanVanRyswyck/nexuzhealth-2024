package ports

type BirthdayGreetingsMessageData struct {
	FirstName string
	Email     string
}

type EmailSender interface {
	SendBirthdayGreetings(messageData BirthdayGreetingsMessageData)
}
