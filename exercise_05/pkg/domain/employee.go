package domain

import (
	"exercise_05/pkg/ports"
	"time"
)

type Employee struct {
	LastName    string
	FirstName   string
	DateOfBirth time.Time
	Email       string
}

func (e *Employee) celebratesBirthdayAt(date time.Time) bool {
	return e.DateOfBirth.Month() == date.Month() && e.DateOfBirth.Day() == date.Day()
}

func MapFrom(employeeData []ports.EmployeeData) []Employee {

	var result []Employee
	for _, dataItem := range employeeData {
		employee := Employee{
			LastName:    dataItem.LastName,
			FirstName:   dataItem.FirstName,
			DateOfBirth: dataItem.DateOfBirth,
			Email:       dataItem.Email,
		}

		result = append(result, employee)
	}

	return result
}
