package domain

import (
	"exercise_05/pkg/ports"
	"time"
)

type BirthdayGreetingsHandler interface {
	SendGreetings(currentDate time.Time) error
}

type birthdayGreetingsHandler struct {
	employeeRepository ports.EmployeeRepository
	emailSender        ports.EmailSender
}

func NewBirthdayGreetingsHandler(employeeRepository ports.EmployeeRepository, emailSender ports.EmailSender) BirthdayGreetingsHandler {
	return &birthdayGreetingsHandler{
		employeeRepository: employeeRepository,
		emailSender:        emailSender,
	}
}

func (h *birthdayGreetingsHandler) SendGreetings(currentDate time.Time) error {
	return nil
}
