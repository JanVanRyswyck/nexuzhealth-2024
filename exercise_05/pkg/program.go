package pkg

import (
	"exercise_05/pkg/domain"
	"exercise_05/pkg/infrastructure"
	"time"
)

func ExecuteProgram(currentDate time.Time, employeesFileName string) {
	employeeRepository := infrastructure.NewFileSystemEmployeeRepository(employeesFileName)

	emailSender, err := infrastructure.NewConsoleEmailSender("noreply@birthdayservice.be", "smtp://birthday.service.be:587")
	if err != nil {
		panic(err)
	}

	birthdayGreetings := domain.NewBirthdayGreetingsHandler(employeeRepository, emailSender)
	err = birthdayGreetings.SendGreetings(currentDate)
	if err != nil {
		panic(err)
	}
}
