package infrastructure

import (
	"encoding/csv"
	"exercise_05/pkg/ports"
	"os"
	"strings"
	"time"
)

type employeeRepository struct {
	fileName string
}

func NewFileSystemEmployeeRepository(fileName string) ports.EmployeeRepository {
	return &employeeRepository{fileName: fileName}
}

func (r *employeeRepository) Get() ([]ports.EmployeeData, error) {

	employeeFile, err := os.Open(r.fileName)
	if err != nil {
		return nil, err
	}

	defer func() { _ = employeeFile.Close() }()

	csvReader := csv.NewReader(employeeFile)

	// Skip the first line of the CSV file (headers)
	_, _ = csvReader.Read()

	employeeRecords, err := csvReader.ReadAll()
	if err != nil {
		return nil, err
	}

	var result []ports.EmployeeData
	for _, employeeRecord := range employeeRecords {
		dateOfBirth, _ := time.Parse("2006/01/02", strings.TrimSpace(employeeRecord[2]))

		employee := ports.EmployeeData{
			LastName:    strings.TrimSpace(employeeRecord[0]),
			FirstName:   strings.TrimSpace(employeeRecord[1]),
			DateOfBirth: dateOfBirth,
			Email:       strings.TrimSpace(employeeRecord[3]),
		}

		result = append(result, employee)
	}

	return result, nil
}
