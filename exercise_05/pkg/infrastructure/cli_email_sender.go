package infrastructure

import (
	"exercise_05/pkg/ports"
	"fmt"
	"net/url"
)

type emailSender struct {
	sender  string
	smptUrl *url.URL
}

func NewConsoleEmailSender(sender string, smptpUrl string) (ports.EmailSender, error) {

	parsedSmtpUrl, err := url.Parse(smptpUrl)
	if err != nil {
		return nil, err
	}

	return &emailSender{sender: sender, smptUrl: parsedSmtpUrl}, nil
}

func (s *emailSender) SendBirthdayGreetings(messageData ports.BirthdayGreetingsMessageData) {
	fmt.Println("----------------------------------------------------------")
	fmt.Printf("Sending message to: %s\n", s.smptUrl)
	fmt.Printf("From: %s\n", s.sender)
	fmt.Printf("To: %s\n", messageData.Email)
	fmt.Println("Subject: Happy birthday!")
	fmt.Printf("Message: Happy birthday, dear %s. Have a wonderful day.\n", messageData.FirstName)
	fmt.Println("----------------------------------------------------------")
}
