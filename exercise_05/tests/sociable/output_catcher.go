package sociable

import (
	"bytes"
	"io"
	"os"
)

type OutputCatcher struct {
	previousStdout *os.File
	reader         *os.File
}

func (oc *OutputCatcher) Start() {
	oc.previousStdout = os.Stdout
	reader, writer, _ := os.Pipe()
	oc.reader = reader
	os.Stdout = writer
}

func (oc *OutputCatcher) Stop() {
	_ = os.Stdout.Close()
	os.Stdout = oc.previousStdout
}

func (oc *OutputCatcher) GetCapturedOutput() string {
	var buffer bytes.Buffer
	_, _ = io.Copy(&buffer, oc.reader)
	return buffer.String()
}
