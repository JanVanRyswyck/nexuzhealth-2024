package sociable

import (
	"testing"
)

/*
 * Write a Sociable test by implementing the Given/When/Then functions.
 *
 * The following employee data:
 *
 * {"last_name", "first_name", "date_of_birth", "email"},
 * {"Simpson", "Homer", "1977/08/12", "homer.simpson@springfieldpowerplant.com"},
 * {"Flanders", "Ned", "1976/12/25", "new.flanders@holychurch.com"},
 * {"Skinner", "Seymour", "1968/04/08", "seymour.skinner@springfieldelementary.com"},
 *
 * should result in the following output:
 *
 * "----------------------------------------------------------\n" +
 * "Sending message to: smtp://birthday.service.be:587\n" +
 * "From: noreply@birthdayservice.be\n" +
 * "To: seymour.skinner@springfieldelementary.com\n" +
 * "Subject: Happy birthday!\n" +
 * "Message: Happy birthday, dear Seymour. Have a wonderful day.\n" +
 * "----------------------------------------------------------\n"
 *
 * Tip: Use the `OutputCatcher` to scrape the output from the console.
 */

type testScenario struct {
	employeeDataFileName  string
	sentBirthdayGreetings string
}

func TestSendBirthdayGreetings(t *testing.T) {
	testScenario{}.
		GivenEmployeeData().
		WhenSendingBirthdayGreetings().
		ItShouldSendAnEmailToTheBirthdayPeople(t)
}

func (ts testScenario) GivenEmployeeData() testScenario {

	//
	// Step 1: Create a csv file with employee data in the temp directory.
	//
	return ts
}

func (ts testScenario) WhenSendingBirthdayGreetings() testScenario {

	//
	// Step 2: Execute the system for sending the birthday greetings.
	// Tip: Use the `OutputCatcher` to scrape the output from the console.
	// Tip: Use the `ExecuteProgram` function to run the system.
	//
	return ts
}

func (ts testScenario) ItShouldSendAnEmailToTheBirthdayPeople(t *testing.T) {

	//
	// Step 3: Assert that the birthday greeting has been sent.
	//
}
