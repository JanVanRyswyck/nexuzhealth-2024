package solitary

/*
 * Implement the Solitary tests for the birthday greetings handler in the following order:
 *
 * It's someone's birthday today:
 *   - email to the employee
 * No birthday's today:
 *   - no emails should be sent
 * An error occurs when retrieving the list of employees:
 *   - returns an error that indicates a failure
 *
 * Tip: Make use of the `EmployeeRepositoryStub`, `EmailSenderSpy` and `EmailSenderDummy` when appropriate while working
 * your way through the tests.
 */
