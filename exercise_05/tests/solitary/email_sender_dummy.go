package solitary

import "exercise_05/pkg/ports"

type EmailSenderDummy struct{}

func (esd *EmailSenderDummy) SendBirthdayGreetings(_ ports.BirthdayGreetingsMessageData) {
	panic("The SendBirthdayGreetings of the EmailSender should not be called.")
}
