package solitary

import "exercise_05/pkg/ports"

type EmployeeRepositoryStub struct {
	Employees []ports.EmployeeData
	Error     error
}

func (ers *EmployeeRepositoryStub) Get() ([]ports.EmployeeData, error) {
	return ers.Employees, ers.Error
}
