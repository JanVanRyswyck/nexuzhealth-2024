package solitary

import "exercise_05/pkg/ports"

type EmailSenderSpy struct {
	ReceivedMessage ports.BirthdayGreetingsMessageData
}

func (ess *EmailSenderSpy) SendBirthdayGreetings(messageData ports.BirthdayGreetingsMessageData) {
	ess.ReceivedMessage = messageData
}
