package expensesheet

import (
	"fmt"
	"time"
)

type ExpenseType int

const (
	DINNER ExpenseType = iota
	BREAKFAST
	CAR_RENTAL
)

type Expense struct {
	Type   ExpenseType
	Amount int
}

func Print(expenses []Expense) {
	total := 0
	mealExpenses := 0

	fmt.Println("Expenses", time.Now().Format("2006-01-02 15:04:05"))

	for _, expense := range expenses {
		if expense.Type == DINNER || expense.Type == BREAKFAST {
			mealExpenses += expense.Amount
		}

		expenseName := ""
		switch expense.Type {
		case DINNER:
			expenseName = "Dinner"
			break
		case BREAKFAST:
			expenseName = "Breakfast"
			break
		case CAR_RENTAL:
			expenseName = "Car rental"
			break
		}

		mealOverExpensesMarker := " "
		if expense.Type == DINNER && expense.Amount > 5000 || expense.Type == BREAKFAST && expense.Amount > 1000 {
			mealOverExpensesMarker = "X"
		}

		fmt.Println(expenseName, "\t", expense.Amount, "\t", mealOverExpensesMarker)

		total += expense.Amount
	}

	fmt.Println("Meal expenses:", mealExpenses)
	fmt.Println("Total expenses:", total)
}
