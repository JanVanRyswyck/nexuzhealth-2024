package test

import (
	approvals "github.com/approvals/go-approval-tests"
	"github.com/approvals/go-approval-tests/reporters"
	"os"
	"regexp"
	"testing"
)
import "exercise_07/pkg/expensesheet"

// Step 1: a first approval test
//
// Create an expense sheet for a single dinner expense with a fairly moderate amount. The output can be verified by calling
// `approvals.VerifyString`. Visually check the output in the "received" file (see approvals folder). When the received
// content matches the expectations, rename "received" to "approved" or sync the content to the "approved" file.
//
// Tip: See the documentation for `approvals.VerifyString`:
// https://pkg.go.dev/github.com/approvals/go-approval-tests#VerifyString
//
// Tip: Unfortunately, the `Print` function doesn't provide any return value. Instead, it directly calls `fmt.Println`.
// This implies that we need to somehow capture the output being written to the console. The `OutputCatcher` in
// `output_catcher.go` enables this for us. Have a look at the implementation in order to understand how  it works.
// You can use the `GetCapturedOutput` function of the OutputCatcher to retrieve the output.
//
// Tip: Make sure that the test passes after approval. If you run into a date/time related issue, have a look
// at the `createDateTimeScrubber` function and the following example:
// https://github.com/approvals/go-approval-tests/blob/434b9105e958695377ea00bac571e15c6b2a19ca/scrubber_test.go#L63

func TestPrintDinnerExpense(t *testing.T) {

}

// Step 2: a more comprehensive approval test
//
// One way to go forward is to write additional approval tests like the one in step 1 to cover every scenario for
// the `print` function. A more comprehensive way is to use `approvals.VerifyAll` to verify all scenarios in one go.
//
// See the documentation for more information:
// https://pkg.go.dev/github.com/approvals/go-approval-tests#VerifyAll
//
// Tip: Expand the `expenseScenarios` function for adding more scenarios.
// Tip: Use a code coverage tool to verify whether all code paths have been executed through the implementation of
// the `print` function.

func TestPrintDinnerScenarios(t *testing.T) {
	options := approvals.Options() // TODO: Add the same date scrubbing as in "Step 1".

	approvals.VerifyAll(t, "", expenseScenarios(), func(i interface{}) string {
		//
		// TODO: call the `print` function and return the captured output
		//
		return ""
	}, options)
}

func expenseScenarios() [][]expensesheet.Expense {
	dinnerOf100 := expensesheet.Expense{Type: expensesheet.DINNER, Amount: 100}

	//
	// TODO: Create more `Expense` instances as needed
	//

	return [][]expensesheet.Expense{
		{dinnerOf100},

		//
		// TODO: Add a list of one or multiple expenses for each scenario.
		//
	}
}

// Step 3: a combinatorial approval test
//
// Let's try a different approach compared to step 2. Another way would be to use a combination of parameters.
// In our case we can create combinations of expense types and amounts. These combinations can be verified using
// `approvals.VerifyAllCombinationsFor2`.
//
// See the documentation for more information:
// https://pkg.go.dev/github.com/approvals/go-approval-tests#VerifyAllCombinationsFor2
//
// Tip: Unfortunately, the `approvals.VerifyAllCombinationsFor2` function doesn't support options. Therefore, you have
// to scrub the output for date/time yourself. You can use the `scrubDateTime` function inside `doPrint` to do this.
//
// Tip: Use a code coverage tool to verify whether all code paths have been executed through the implementation of
// the `print` function.

func TestPrintExpenseCombinations(t *testing.T) {
	expenseTypes := []expensesheet.ExpenseType{}
	amounts := []int{}

	doPrint := func(expenseType interface{}, amount interface{}) string {
		//
		// TODO: Create an `Expense` instance using the specified parameters, call the `print` function and return the
		// captured output.
		//

		return ""
	}

	approvals.VerifyAllCombinationsFor2(t, "", doPrint, expenseTypes, amounts)
}

// Step 4: take some time to review the tests for step 2 and step 3. What are the differences between these tests?
// Which approach is best suited in our case? Why?

func createDateTimeScrubber() *regexp.Regexp {
	scrubber, _ := regexp.Compile("\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}")
	return scrubber
}

func scrubDateTime(input string) string {
	scrubber := createDateTimeScrubber()
	return scrubber.ReplaceAllString(input, "<time>")
}

func TestMain(m *testing.M) {
	approvals.UseReporter(reporters.NewMultiReporter(reporters.NewDiffReporter(), reporters.NewClipboardReporter()))
	approvals.UseFolder("approvals")
	os.Exit(m.Run())
}
