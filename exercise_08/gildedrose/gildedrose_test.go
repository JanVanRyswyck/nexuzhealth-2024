package gildedrose_test

import (
	"fmt"
	approvals "github.com/approvals/go-approval-tests"
	"github.com/emilybache/gildedrose-refactoring-kata/gildedrose"
	"testing"
)

func TestGildedRoseScenarios(t *testing.T) {
	names := []string{"Aged Brie", "Sulfuras, Hand of Ragnaros", "Backstage passes to a TAFKAL80ETC concert", "Unknown"}
	sellIns := []int{-1, 0, 5, 6, 10, 11}
	qualities := []int{0, 1, 49, 50}

	performUpdateQuality := func(name interface{}, sellIn interface{}, quality interface{}) string {
		item := gildedrose.Item{Name: name.(string), SellIn: sellIn.(int), Quality: quality.(int)}
		gildedrose.UpdateQuality([]*gildedrose.Item{&item})
		return fmt.Sprintf("%#v", item)
	}

	approvals.VerifyAllCombinationsFor3(t, "", performUpdateQuality, names, sellIns, qualities)
}
