# Hotel Booking

## 1. Description

We're entering the realm of the hospitality business. For that our company needs a hotel management service. As its very
first feature, it should be possible for the hotel manager to register a new hotel in the system or change the name of 
an existing hotel due to rebranding or a takeover.

In order to be web scale, the architect decided to use a SQLite database for data storage. The only table in the database
(for the time being) has been named `Hotel`. It has the following schema:
```
Id, Name
```
## 2. Sociable tests

### 2.1 SqliteHotelRepository / FakeHotelRepository

1. Open the files `tests/sociable/hotel_repository_test.go`, `pkg/infrastructure/sqlite_hotel_repository.go` and 
`tests/sociable/fake_hotel_repository.go`.
2. By implementing the contract tests, we're going to flesh out the implementation of both the `SqliteHotelRepository`
and `FakeHotelRepository`. 
3. Follow the steps as outlined in the comments. Start with the first Solitary test from the list, following a Test-First 
approach ("Red, Green, Refactor").
   * Implement only a single test case at a time. Make sure that the test fails.
   * Provide the least amount of code to make the test pass.
   * Refactor, refactor, refactor!
   * Rinse & repeat.

### 2.2 ChangeHotelName

1. Open the file `tests/sociable/change_hotel_name_test.go`.
2. Implement the Sociable test by using the `FakeHotelRepository`. Follow the steps as outlined in the comments.

## 3. Open questions

* In `hotel_repository_test.go`, we have a test that verifies whether a registered hotel can be retrieved. It first 
persists a new hotel in the repository using the `Save` method. After that, the persisted hotel is retrieved again using 
`Get` method. What could be a (potential) downside of using the Subject Under Test for both persisting and retrieving a 
hotel? How could we further improve this?

* The test in `change_hotel_name_test.go` now uses the fake repository instead of the repository that access the database. 
This should improve both the stability and the performance of these tests. Are there any other advantages that you see 
when using this approach? Are there also any disadvantages?

## 4. Bonus exercise

Add a new feature that also stores rooms for a particular hotel. A room has unique room number and a type (standard, 
queen suite, master suite, presidential suite). A room can be added or its type can be changed.

The `Room` table in the database can have the following schema:
```
Number, HotelId, Type
```
