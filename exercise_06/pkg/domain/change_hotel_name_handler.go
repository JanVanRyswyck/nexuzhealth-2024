package domain

import (
	"errors"
	"exercise_06/pkg/ports"
	"github.com/google/uuid"
)

type ChangeHotelNameHandler interface {
	ChangeHotelName(hotelId uuid.UUID, hotelName string) error
}

type changeHotelNameHandler struct {
	hotelRepository ports.HotelRepository
}

func NewChangeHotelNameHandler(hotelRepository ports.HotelRepository) ChangeHotelNameHandler {
	return &changeHotelNameHandler{hotelRepository: hotelRepository}
}

func (h changeHotelNameHandler) ChangeHotelName(hotelId uuid.UUID, newHotelName string) error {
	hotelData := h.hotelRepository.Get(hotelId)
	if hotelData == nil {
		return errors.New("unknown hotel")
	}

	hotelData.Name = newHotelName
	return h.hotelRepository.Save(*hotelData)
}
