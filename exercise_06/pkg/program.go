package pkg

import (
	"exercise_06/pkg/domain"
	"exercise_06/pkg/infrastructure"
	"exercise_06/pkg/ports"
	"github.com/google/uuid"
)

type Program struct {
	handler domain.ChangeHotelNameHandler
}

func NewProgram(hotelRepository ports.HotelRepository) *Program {
	if hotelRepository == nil {
		database, err := infrastructure.Connect()
		if err != nil {
			panic(err)
		}

		hotelRepository = infrastructure.NewSqliteHotelRepository(database)
	}

	handler := domain.NewChangeHotelNameHandler(hotelRepository)
	return &Program{handler: handler}
}

func (p Program) ExecuteProgram(hotelId string, newHotelName string) {
	parsedHotelId, err := uuid.Parse(hotelId)
	handle(err)

	err = p.handler.ChangeHotelName(parsedHotelId, newHotelName)
	handle(err)
}

func handle(err error) {
	if err == nil {
		return
	}

	panic(err)
}
