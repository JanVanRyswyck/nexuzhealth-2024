package infrastructure

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

func Connect() (*sql.DB, error) {
	connectionString := "database/hotel_prod.db"
	return sql.Open("sqlite3", connectionString)
}
