package infrastructure

import (
	"database/sql"
	"exercise_06/pkg/ports"
	"github.com/google/uuid"
)

type sqliteHotelRepository struct {
	database *sql.DB
}

func NewSqliteHotelRepository(database *sql.DB) ports.HotelRepository {
	return &sqliteHotelRepository{database: database}
}

func (r *sqliteHotelRepository) Get(id uuid.UUID) *ports.HotelData {
	const SQL = "SELECT Name FROM Hotel WHERE id = ?"

	// Tip: Make sure to close the "rows" returned by `database.Query(...)` .
	return nil
}

func (r *sqliteHotelRepository) Save(hotelData ports.HotelData) error {
	const SQL_INSERT = "INSERT INTO Hotel (Id, Name) VALUES (?, ?)"
	const SQL_UPDATE = "UPDATE Hotel SET Name = ? WHERE Id = ?"

	return nil
}
