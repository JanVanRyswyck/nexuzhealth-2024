package ports

import (
	"github.com/google/uuid"
)

type HotelData struct {
	Id   uuid.UUID
	Name string
}

type HotelRepository interface {
	Get(id uuid.UUID) *HotelData
	Save(hotelData HotelData) error
}
