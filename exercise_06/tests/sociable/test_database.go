package sociable

import (
	"database/sql"
	"fmt"
	"io"
	"os"
	"path"

	_ "github.com/mattn/go-sqlite3"
)

type TestDatabase struct {
	database *sql.DB
}

func NewTestDatabase() TestDatabase {
	return TestDatabase{}
}

func (td *TestDatabase) Initialise() (*sql.DB, error) {
	database, err := createDatabase()
	if err != nil {
		return nil, err
	}
	td.database = database

	err = executeScript("../../database/InitialiseSchema.sql", database)
	if err != nil {
		return nil, err
	}

	return database, nil
}

func (td *TestDatabase) Cleanup() error {
	if td.database == nil {
		return nil
	}

	err := executeScript("../../database/CleanupSchema.sql", td.database)
	if err != nil {
		return err
	}

	err = td.database.Close()
	if err != nil {
		return err
	}

	td.database = nil
	return nil
}

func createDatabase() (*sql.DB, error) {
	databaseFileName := path.Join(os.TempDir(), "hotel.db")
	databaseFile, _ := os.Create(databaseFileName)
	_ = databaseFile.Close()

	connectionString := fmt.Sprintf("%s:?_foreign_keys=true", databaseFileName)

	database, err := sql.Open("sqlite3", connectionString)
	if err != nil {
		return nil, err
	}

	return database, nil
}

func executeScript(sqlScriptFileName string, database *sql.DB) error {
	scriptFile, err := os.Open(sqlScriptFileName)
	if err != nil {
		return err
	}

	scriptFileData, err := io.ReadAll(scriptFile)
	if err != nil {
		return err
	}

	_, err = database.Exec(string(scriptFileData))
	if err != nil {
		return err
	}
	return nil
}
