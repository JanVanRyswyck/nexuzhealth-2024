package sociable

import (
	"exercise_06/pkg/ports"
	"github.com/google/uuid"
	"testing"
)

/*
 * Write a Sociable test by implementing the Given/When/Then functions.
 */

type testScenario struct {
	hotelRepository ports.HotelRepository
	hotelId         uuid.UUID
}

func TestChangeHotelName(t *testing.T) {
	testScenario{}.
		GivenAnHotel().
		WhenANewNameIsAssumed().
		ItShouldChangeTheNameOfTheHotel(t)
}

func (ts testScenario) GivenAnHotel() testScenario {

	//
	// Step 1: Create a new hotel in the `FakeHotelRepository`.
	//

	return ts
}

func (ts testScenario) WhenANewNameIsAssumed() testScenario {

	//
	// Step 2: Create a new instance of the `Program` and perform the `ExecuteProgram` function.
	// Tip: Specify the `FakeHotelRepository` when creating a new `Program`.
	//

	return ts
}

func (ts testScenario) ItShouldChangeTheNameOfTheHotel(t *testing.T) {

	//
	// Step 3: Assert that the hotel name has been changed in the `FakeHotelRepository`.
	//

}
