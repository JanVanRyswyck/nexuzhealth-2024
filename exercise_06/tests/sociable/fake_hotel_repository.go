package sociable

import (
	"exercise_06/pkg/ports"
	"github.com/google/uuid"
)

type fakeHotelRepository struct {
	hotels map[uuid.UUID]ports.HotelData
}

func NewFakeHotelRepository() ports.HotelRepository {
	return &fakeHotelRepository{hotels: make(map[uuid.UUID]ports.HotelData)}
}

func (r fakeHotelRepository) Get(id uuid.UUID) *ports.HotelData {
	return nil
}

func (r fakeHotelRepository) Save(hotelData ports.HotelData) error {
	return nil
}
