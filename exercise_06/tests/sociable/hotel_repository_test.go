package sociable

import (
	"exercise_06/pkg/infrastructure"
	"exercise_06/pkg/ports"
	"testing"
)

func provideSUTStrategies() []HotelRepositoryStrategy {
	return []HotelRepositoryStrategy{
		// !! START HERE !!
		// One way to pass arguments to test methods is by implementing a custom `ArgumentsProvider`. In this
		// case we create a `SQLiteHotelRepositoryStrategy` and an `FakeHotelRepositoryStrategy`. Have a look
		// at the implementation of these two classes at the bottom of this source file.
		//
		// Uncomment the following two lines of code:
		//&SQLiteHotelRepositoryStrategy{},
		//&FakeHotelRepositoryStrategy{},
	}
}

func TestHotelRepository_Get_ShouldRetrieveHotel(t *testing.T) {
	// Test 1: Observe a hotel can be retrieved from the database

	for _, strategy := range provideSUTStrategies() {
		// 1.1 - Create the Subject Under Test using the strategy.
		// Tip: Make sure to also cleanup.
		_ = strategy.GetSubjectUnderTest()

		// 1.2 - Create a new hotel using the `Save` method of the Subject Under Test.

		// 1.3 - Retrieve the created hotel using the `Get` method of the Subject Under Test. Observe that the retrieved
		// hotel data matches the saved hotel data.
	}
}

func TestHotelRepository_GetNonExistingHotel_ShouldReturnNothing(t *testing.T) {
	// Test 2: Observe whether a hotel that doesn't exist in the database returns a nil reference.

	for _, strategy := range provideSUTStrategies() {
		// 2.1 - Create the Subject Under Test using the strategy.
		// Tip: Make sure to also cleanup.
		_ = strategy.GetSubjectUnderTest()

		// 2.2 - Use the `Get` method of the Subject Under Test to retrieve the hotel.
	}
}

func TestHotelRepository_SaveExistingHotel_ShouldChangeTheHotelName(t *testing.T) {
	// Test 3: Observe that the name of an existing hotel can be changed in the database.

	for _, strategy := range provideSUTStrategies() {
		// 3.1 - Create the Subject Under Test using the strategy.
		// Tip: Make sure to also cleanup.
		_ = strategy.GetSubjectUnderTest()

		// 3.2 - Create a new hotel using the `Save` method of the Subject Under Test.

		// 3.3 - Change the name of the hotel and save the hotel data in the database.

		// 3.4 - Retrieve the saved hotel using the `Get` method of the Subject Under Test. Observe that the retrieved
		// hotel data matches the saved hotel data.
	}
}

type HotelRepositoryStrategy interface {
	GetSubjectUnderTest() ports.HotelRepository
	Cleanup()
}

// SQLite HotelRepository strategy
type SQLiteHotelRepositoryStrategy struct {
	testDatabase TestDatabase
}

func (s *SQLiteHotelRepositoryStrategy) GetSubjectUnderTest() ports.HotelRepository {
	s.testDatabase = NewTestDatabase()
	database, err := s.testDatabase.Initialise()
	if err != nil {
		panic(err)
	}

	return infrastructure.NewSqliteHotelRepository(database)
}

func (s *SQLiteHotelRepositoryStrategy) Cleanup() {
	err := s.testDatabase.Cleanup()
	if err != nil {
		panic(err)
	}
}

// Fake HotelRepository strategy
type FakeHotelRepositoryStrategy struct{}

func (*FakeHotelRepositoryStrategy) GetSubjectUnderTest() ports.HotelRepository {
	return NewFakeHotelRepository()
}

func (*FakeHotelRepositoryStrategy) Cleanup() {}
