package main

import (
	"exercise_06/pkg"
)

func main() {

	hotelId := "3d420835-f9dd-4deb-96a1-b33ea9f94472"
	newHotelName := "The Bellagio"

	program := pkg.NewProgram(nil)
	program.ExecuteProgram(hotelId, newHotelName)
}
