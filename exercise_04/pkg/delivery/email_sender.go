package delivery

import (
	"fmt"
	"net/url"
)

type EmailSender interface {
	Send(email string, subject string, message string)
}

type ConsoleEmailSender struct {
	sender  string
	smptUrl *url.URL
}

func (ces *ConsoleEmailSender) Send(email string, subject string, message string) {
	fmt.Println("----------------------------------------------------------")
	fmt.Printf("Sending message to server: %s\n", ces.smptUrl)
	fmt.Printf("From: %s\n", ces.sender)
	fmt.Printf("To: %s\n", email)
	fmt.Printf("Subject: %s\n", subject)
	fmt.Printf("Message: %s\n", message)
	fmt.Println("----------------------------------------------------------")
}
