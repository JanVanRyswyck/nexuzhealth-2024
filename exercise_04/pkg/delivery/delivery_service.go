package delivery

import (
	"time"
)

type DeliveryEvent struct {
	Id             int64
	TimeOfDelivery time.Time
}

type DeliveryController struct {
	DeliveryRepository DeliveryRepository
	EmailSender        EmailSender
}

func (dc *DeliveryController) UpdateDelivery(deliveryEvent DeliveryEvent) error {
	return nil
}
