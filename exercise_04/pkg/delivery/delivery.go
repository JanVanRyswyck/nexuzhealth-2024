package delivery

import "time"

type Delivery struct {
	Id             int64
	ContactEmail   string
	TimeOfDelivery time.Time
	Arrived        bool
	OnTime         bool
}
