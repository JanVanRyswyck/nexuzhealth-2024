package delivery

import "slices"

type DeliveryRepository interface {
	Add(delivery Delivery)
	Get(id int64) *Delivery
}

type InMemoryDeliveryRepository struct {
	deliveries []Delivery
}

func (ds *InMemoryDeliveryRepository) Add(delivery Delivery) {
	ds.deliveries = append(ds.deliveries, delivery)
}

func (ds *InMemoryDeliveryRepository) Get(id int64) *Delivery {
	index := slices.IndexFunc(ds.deliveries, func(delivery Delivery) bool { return delivery.Id == id })
	if index == -1 {
		return nil
	}

	return &ds.deliveries[index]
}
