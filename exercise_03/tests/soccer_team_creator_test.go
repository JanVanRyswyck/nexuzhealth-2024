package tests

/*
 * Implement the Solitary tests for the soccer team creator in the following order:
 *
 * Happy path - create new soccer team
 * Invalid UUID - returns an error that indicates a bad identifier
 * Before the start of the authorised composition period - returns an error
 * After the end of the authorised composition period - returns an error
 */
