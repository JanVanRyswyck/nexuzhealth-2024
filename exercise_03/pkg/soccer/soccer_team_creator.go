package soccer

import (
	"github.com/google/uuid"
	"time"
)

type Season struct {
	StartYear int
	EndYear   int
}

type SoccerTeam struct {
	Id         uuid.UUID
	ClubNumber int
	Name       string
	Season     Season
}

func CreateSoccerTeam(id string, clubNumber int, name string, currentDate time.Time) (*SoccerTeam, error) {
	return nil, nil
}
