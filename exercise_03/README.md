# Bank account

## 1. Description

In a system that manages the administration of soccer clubs, there's a [factory](https://en.wikipedia.org/wiki/Factory_method_pattern)
that can create a soccer team. A soccer team has a unique identifier, a name, for which club it's being registered and
for which season it's going to compete with other teams.

The soccer association imposes on clubs that a team can only be composed after the end of one season and the
beginning of a new season (between May 1 and August 31). When the current date is before or after the allowed 
composition period, then the `CreateSoccerTeam` function should return an error.

## 2. Solitary tests

1. Open the files `pkg/soccer/soccer_team_creator.go` and `tests/soccer_team_creator_test.go`.
2. Start with the first Solitary test from the list, following a Test-First approach ("Red, Green, Refactor").
   * Implement only a single test case at a time. Make sure that the test fails.
   * Provide the least amount of code to make the test pass.
   * Refactor, refactor, refactor!
   * Rinse & repeat.

## 3. Open questions

* What are your thoughts about “Object State Verification” compared to “Procedural State Verification”.
* In which cases would you use one approach over the other? Why?

