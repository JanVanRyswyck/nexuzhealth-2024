package solitary

import "time"

type ClockStub struct {
	CurrentDateTime time.Time
}

func (cs *ClockStub) GetCurrentDateTime() time.Time {
	return cs.CurrentDateTime
}
