package solitary

import (
	"exercise_01/pkg/bank"
)

type TransactionsSpy struct {
	AddedTransaction       bank.Transaction
	TransactionsToRetrieve []bank.Transaction
}

func (ts *TransactionsSpy) Add(transaction bank.Transaction) {
	ts.AddedTransaction = transaction
}

func (ts *TransactionsSpy) Get() []bank.Transaction {
	return ts.TransactionsToRetrieve
}
