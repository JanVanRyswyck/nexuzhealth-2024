package solitary

type PrinterSpy struct {
	PrintedLines []string
}

func (ps *PrinterSpy) PrintLine(line string) {
	ps.PrintedLines = append(ps.PrintedLines, line)
}
