package sociable

import "time"

type FakeClock struct {
	DateTimes []time.Time
	index     int
}

func (fc *FakeClock) GetCurrentDateTime() time.Time {
	if fc.index >= len(fc.DateTimes) {
		panic("FakeClock is requested more date/time values than has been provided!")
	}

	result := fc.DateTimes[fc.index]
	fc.index += 1
	return result
}
