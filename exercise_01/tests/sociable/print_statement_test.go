package sociable

import (
	"testing"
)

func TestPrintStatement_ShouldOutputAllTransactions(t *testing.T) {
	/*
	 * Write a Sociable test for the following expected output:
	 * expectedOutput := "DateTime | Amount | Balance\n" +
	 *	"2024-03-16 | 150 | 250\n" +
	 *	"2024-03-15 | -100 | 100\n" +
	 *	"2024-03-14 | 200 | 200\n"
	 *
	 * Tip: Use the `OutputCatcher` to scrape the output from the console.
	 */
}
