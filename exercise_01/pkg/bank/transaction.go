package bank

import "time"

type Transaction struct {
	Amount   int
	DateTime time.Time
}
