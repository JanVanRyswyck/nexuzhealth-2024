package bank

import "fmt"

type Printer interface {
	PrintLine(line string)
}

type ConsolePrinter struct{}

func (p *ConsolePrinter) PrintLine(line string) {
	fmt.Println(line)
}
