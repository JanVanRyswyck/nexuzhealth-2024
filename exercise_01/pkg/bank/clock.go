package bank

import "time"

type Clock interface {
	GetCurrentDateTime() time.Time
}

type SystemClock struct{}

func (c *SystemClock) GetCurrentDateTime() time.Time {
	return time.Now()
}
