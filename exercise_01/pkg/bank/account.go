package bank

type Account struct {
	Clock            Clock
	Transactions     Transactions
	StatementPrinter StatementPrinter
}

func (a *Account) Deposit(amount int) {

}

func (a *Account) Withdraw(amount int) {

}

func (a *Account) PrintStatement() {

}
