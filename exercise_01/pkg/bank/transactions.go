package bank

type Transactions interface {
	Add(transaction Transaction)
	Get() []Transaction
}

type TransactionsStore struct {
	transactions []Transaction
}

func (t *TransactionsStore) Add(transaction Transaction) {
	t.transactions = append(t.transactions, transaction)
}

func (t *TransactionsStore) Get() []Transaction {
	return t.transactions
}
