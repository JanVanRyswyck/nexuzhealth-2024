package main

import (
	"exercise_01/pkg/bank"
)

func main() {

	clock := &bank.SystemClock{}
	transactions := &bank.TransactionsStore{}
	statementPrinter := bank.StatementPrinter{Printer: &bank.ConsolePrinter{}}

	account := bank.Account{Clock: clock, Transactions: transactions, StatementPrinter: statementPrinter}

	account.Deposit(200)
	account.Withdraw(100)
	account.Deposit(150)
	account.PrintStatement()
}
