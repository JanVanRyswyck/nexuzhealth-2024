# Bank account

## 1. Description

Create a simple bank application with the following features:
* Deposit into Account
* Withdraw from an Account
* Print a bank statement to the console

## 2. Sociable test

1. Open the file `tests/sociable/print_statement_test.go`.
2. Implement a failing Sociable test. 
3. Do not yet add production code to make the Sociable test pass. Complete the implementation of the Solitary tests 
first (see next section).

## 3. Solitary tests

1. Open the file `tests/solitary/account_test.go`.
2. Complete each Solitary test one by one using a Test-First approach ("Red, Green, Refactor").
   * Implement only a single test case at a time. Make sure that the test fails.
   * Provide the least amount of code to make the test pass.
   * Refactor, refactor, refactor!
   * Repeat these steps until all the tests pass.
3. When all Solitary tests are completed, run the Sociable test in `print_statement_test.go` to see if it passes. 

## 4. Open questions

* Which kind of tests gave you the fastest feedback? The Solitary tests or the single Sociable test?
* Can you make an observation regarding the ratio between Solitary tests and Sociable tests?
* Did you choose a concrete instance of the "StatementPrinter"? Or did you use a test double? What would be the
  implications of making a different choice?
