# Shopping Basket

## 1. Description

Items in a shopping basket have a unit price and quantity. Write code that will allow you to:

* Find out the quantity of a particular item in the basket
* Calculate the total price of the whole basket, including any applicable discount

Normally the total price is the sum of unit price * quantity for all the items. If you buy in bulk you get a 
discount:

* If total basket value > $100, apply a 5% discount
* If total basket value > $200, apply a 10% discount

## 2. Solitary tests

1. Open the file `tests/shopping_basket_test.go`. This file contains a list of Solitary tests to implement.
2. Start with the first Solitary test from the list, following a Test-First approach ("Red, Green, Refactor").
   * Implement only a single test case at a time. Make sure that the test fails.
   * Provide the least amount of code to make the test pass.
   * Refactor, refactor, refactor!
   * Rinse & repeat.

## 3. Open questions

* In your own words, explain why using the Arrange-Act-Assert pattern improves test maintainability.
* Which approach did you use to separate each stage of the Arrange-Act-Assert pattern?
* What's your favourite style for naming tests? Why?
