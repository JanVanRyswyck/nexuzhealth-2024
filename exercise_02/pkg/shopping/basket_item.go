package shopping

type BasketItem struct {
	Name  string
	Price float64
}
