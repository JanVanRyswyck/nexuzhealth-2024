package tests

/*
 * Implement the Solitary tests for the shopping basket in the following order:
 *
 * Empty basket - basket contains no items
 * One item "A" - basket contains 1 item "A"
 * Two items "A" - basket contains 2 items "A"
 * Two items, "A" and "B" - basket contains 1 item "A", and contains 1 item of "B"
 * Empty basket - total price 0
 * Item "A" costs $9.99, basket contains one item "A" - total $9.99
 * Item "D" costs $120, basket contains one item "D" - total $114 (5% discount)
 * Item "E" costs $235, basket contains one item "E" - total $211.5 (10% discount)
 */
